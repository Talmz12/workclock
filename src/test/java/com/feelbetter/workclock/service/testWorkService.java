package com.feelbetter.workclock.service;

import com.feelbetter.workclock.model.db.Event;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

public class testWorkService {

    private static final LocalDateTime startDate = LocalDate.of(2020, 10, 1).atTime(LocalTime.MIN);
    private static final int DAYS_IN_YEAR = 365;

    @Test
    public void testNormalDates() {
        LocalDateTime endDate = startDate.toLocalDate().atTime(20, 20, 20);
        List<Event> events = WorkService.generateEventsFromDates(startDate, endDate, "");

        Assert.assertEquals(1, events.size());
        Assert.assertEquals(startDate, events.get(0).getEventKey().getStartTime());
        Assert.assertEquals(endDate, events.get(0).getEndTime());
    }

    @Test
    public void testOneDayViolation() {
        LocalDateTime endDate = startDate.plusDays(1).toLocalDate().atTime(LocalTime.NOON);
        List<Event> events = WorkService.generateEventsFromDates(startDate, endDate, "");

        Assert.assertEquals(2, events.size());
        Assert.assertEquals(startDate, events.get(0).getEventKey().getStartTime());
        Assert.assertEquals(startDate.toLocalDate().atTime(LocalTime.MAX), events.get(0).getEndTime());
        Assert.assertEquals(startDate.plusDays(1).toLocalDate().atStartOfDay(), events.get(1).getEventKey().getStartTime());
        Assert.assertEquals(endDate, events.get(1).getEndTime());
    }

    @Test
    public void testFiveDaysViolation() {
        LocalDateTime endDate = startDate.plusDays(5).toLocalDate().atTime(LocalTime.NOON);
        List<Event> events = WorkService.generateEventsFromDates(startDate, endDate, "");

        Assert.assertEquals(6, events.size());
    }

    @Test
    public void testYearViolation() {
        LocalDateTime endDate = startDate.plusYears(1);
        List<Event> events = WorkService.generateEventsFromDates(startDate, endDate, "");

        Assert.assertEquals(DAYS_IN_YEAR + 1, events.size());
    }
}
