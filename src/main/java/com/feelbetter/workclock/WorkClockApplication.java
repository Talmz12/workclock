package com.feelbetter.workclock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkClockApplication {

    public static void main(String[] args) {
        SpringApplication.run(WorkClockApplication.class, args);
    }

}
