package com.feelbetter.workclock.api;

import com.feelbetter.workclock.model.requests.Person;
import com.feelbetter.workclock.model.requests.ReportPerMonth;
import com.feelbetter.workclock.service.IWorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.logging.Logger;

@RequestMapping(path = "api/v1/worklog", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
@RestController()
public class WorkController {
    private static final Logger logger = Logger.getLogger(WorkController.class.getName());

    @Autowired
    private IWorkService workService;

    @PostMapping
    public void updateTimer(@Valid @RequestBody Person person) {
        logger.info("Got a new update timer request for user: " + person.getUserName());
        workService.handleWorkLog(person.getUserName());
    }

    @GetMapping(path = "/{year}/{month}/{user}")
    public ResponseEntity<ReportPerMonth> getReport(@PathVariable("year") int year, @PathVariable("month") int month, @PathVariable("user") String user) {
        logger.info("Got a new get report request for user: " + user + " for " + year + "/" + month);
        if (month > 12 || month < 1) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return workService.getUserReportPerMonth(user, year, month)
                .map((timePerMonth -> new ResponseEntity(timePerMonth, HttpStatus.ACCEPTED)))
                .orElseGet(() -> new ResponseEntity(HttpStatus.NOT_FOUND));
    }

    @GetMapping(path = "/{year}/{month}")
    public ResponseEntity<List<ReportPerMonth>> getReportForAllUsers(@PathVariable("year") int year, @PathVariable("month") int month) {
        logger.info("Got a new get report request for all users for " + year + "/" + month);
        if (month > 12 || month < 1) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity(this.workService.getAllUsersReportsPerMonth(year, month), HttpStatus.OK);
    }
}
