package com.feelbetter.workclock.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TimePerMonth {
    private double time;
    private String user;

    public TimePerMonth(@JsonProperty("time") double time, @JsonProperty("user") String user) {
        this.user = user;
        this.time = time;
    }
}
