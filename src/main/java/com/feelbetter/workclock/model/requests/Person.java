package com.feelbetter.workclock.model.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.lang.NonNull;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class Person {
    @NotNull
    private String userName;

    public Person(@JsonProperty("user") String userName) {
        this.userName = userName;
    }
}
