package com.feelbetter.workclock.model.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@NoArgsConstructor
public class ReportPerMonth {
    private String user;
    double monthTotal;
    private List<DailyReport> dailyReportList;

    public ReportPerMonth(@JsonProperty("user") String user, @JsonProperty("month_total") double monthTotal, @JsonProperty("daily_report") List<DailyReport> dailyReportList) {
        this.user = user;
        this.monthTotal = monthTotal;
        this.dailyReportList = dailyReportList;
    }
}
