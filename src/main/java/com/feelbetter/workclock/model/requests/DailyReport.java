package com.feelbetter.workclock.model.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class DailyReport {
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private double total;

    public DailyReport(@JsonProperty("start_time") LocalDateTime startTime, @JsonProperty("end_time") LocalDateTime endTime, @JsonProperty("total") double total) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.total = total;
    }
}
