package com.feelbetter.workclock.model.db;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@Data
@Table(name = "events", indexes = {@Index(name = "user_event", columnList = "user, startTime, endTime")
        , @Index(name = "latest_user_log", columnList = "user, startTime")})
@EqualsAndHashCode
public class Event {
    @EmbeddedId
    private EventKey eventKey;
    private LocalDateTime endTime = null;
    private double total;

    public Event(String user, LocalDateTime startTime) {
        this.eventKey = new EventKey(user, startTime);
    }

    public Event(LocalDateTime startTime, LocalDateTime endTime, double total, String user) {
        this(user, startTime);
        this.endTime = endTime;
        this.total = total;
    }
}
