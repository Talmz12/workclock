package com.feelbetter.workclock.model.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MonthlyWorkKey implements Serializable {
    private int year;
    private int month;
    private String user;
}
