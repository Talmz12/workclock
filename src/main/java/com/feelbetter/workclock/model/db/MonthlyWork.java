package com.feelbetter.workclock.model.db;

import com.feelbetter.workclock.model.db.MonthlyWorkKey;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = "monthly_work", indexes = {@Index(name = "user_month", columnList = "user, year, month")})
@EqualsAndHashCode
public class MonthlyWork {
    @EmbeddedId
    private MonthlyWorkKey workKey;
    private double time;
}
