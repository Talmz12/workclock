package com.feelbetter.workclock.service;

import com.feelbetter.workclock.datasource.IEventsCollector;
import com.feelbetter.workclock.datasource.IMonthlyWorkCollector;
import com.feelbetter.workclock.datasource.IUsersCollector;
import com.feelbetter.workclock.model.db.Event;
import com.feelbetter.workclock.model.db.MonthlyWork;
import com.feelbetter.workclock.model.db.MonthlyWorkKey;
import com.feelbetter.workclock.model.TimePerMonth;
import com.feelbetter.workclock.model.db.User;
import com.feelbetter.workclock.model.requests.DailyReport;
import com.feelbetter.workclock.model.requests.ReportPerMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class WorkService implements IWorkService {
    @Autowired
    private IEventsCollector eventsCollector;
    @Autowired
    private IMonthlyWorkCollector workCollector;
    @Autowired
    private IUsersCollector usersCollector;

    private static LocalDateTime getEndOfDay(LocalDate time) {
        return time.atTime(LocalTime.MAX);
    }

    private static double calculateWorkPerDay(LocalDateTime startDate, LocalDateTime endDate) {
        return (LocalDateTime.from(startDate).until(endDate, ChronoUnit.MINUTES)) / 60.0;
    }

    static List<Event> generateEventsFromDates(LocalDateTime startDate, LocalDateTime endDate, String user) {
        List<Event> events = new LinkedList<>();

        // test if end and start on the same day
        if (startDate.toLocalDate().atStartOfDay().isEqual(endDate.toLocalDate().atStartOfDay())) {
            events.add(new Event(startDate, endDate, calculateWorkPerDay(startDate, endDate), user));
            return events;
        }

        // in case the update timer happened after a day passed, we need
        // to calculate the event logs for each day

        // add current day
        events.add(new Event(startDate, getEndOfDay(startDate.toLocalDate()), calculateWorkPerDay(startDate, getEndOfDay(startDate.toLocalDate())), user));

        LocalDateTime currDay = startDate.toLocalDate().atStartOfDay().plusDays(1);
        while (!currDay.toLocalDate().atStartOfDay().isEqual(endDate.toLocalDate().atStartOfDay())) {
            // do something
            events.add(new Event(currDay, getEndOfDay(currDay.toLocalDate()), calculateWorkPerDay(currDay, getEndOfDay(currDay.toLocalDate())), user));
            currDay = currDay.toLocalDate().atStartOfDay().plusDays(1);
        }

        events.add(new Event(currDay, endDate, calculateWorkPerDay(currDay, endDate), user));

        return events;
    }

    @Override
    public void handleWorkLog(String user) {
        Event event = eventsCollector.getLatestUpdateForUser(user);
        LocalDateTime endDate = LocalDateTime.now();
        this.usersCollector.save(new User(user));
        if (event == null || event.getEndTime() != null) {
            eventsCollector.save(new Event(user, endDate));
        } else {
            generateEventsFromDates(event.getEventKey().getStartTime(), endDate, user).forEach((eventPerDay) -> {
                MonthlyWorkKey monthlyWorkKey = new MonthlyWorkKey(eventPerDay.getEventKey().getStartTime().getYear(), eventPerDay.getEventKey().getStartTime().getMonthValue(), user);
                MonthlyWork monthlyWork = workCollector.getMonthlyWork(eventPerDay.getEventKey().getStartTime().getYear(), eventPerDay.getEventKey().getStartTime().getMonthValue(), user);

                workCollector.save(new MonthlyWork(monthlyWorkKey, eventPerDay.getTotal() + (monthlyWork == null ? 0 : monthlyWork.getTime())));
                eventsCollector.save(eventPerDay);
            });
        }
    }

    @Override
    public Optional<ReportPerMonth> getUserReportPerMonth(String user, int year, int month) {
        LocalDate firstDayOfMonth = LocalDate.of(year, month, 1);
        LocalDate lastDayOfMonth = LocalDate.of(year, month, firstDayOfMonth.lengthOfMonth());
        List<DailyReport> dailyReports = this.eventsCollector.getUserEventsPerMonth(user,
                firstDayOfMonth.atTime(LocalTime.MIN), lastDayOfMonth.atTime(LocalTime.MAX))
                .stream()
                .map((event) -> new DailyReport(event.getEventKey().getStartTime(), event.getEndTime(), event.getTotal()))
                .collect(Collectors.toList());
        MonthlyWork monthlyWork = this.workCollector.getMonthlyWork(year, month, user);
        return Optional.ofNullable(monthlyWork).map((mw) -> new ReportPerMonth(user, monthlyWork.getTime(), dailyReports));
    }

    @Override
    public List<ReportPerMonth> getAllUsersReportsPerMonth(int year, int month) {
        List<ReportPerMonth> reportPerMonths = new LinkedList<>();
        this.usersCollector
                .getLatestUpdateForUser()
                .forEach((user -> getUserReportPerMonth(user.getUser(), year, month)
                .ifPresent((report) -> reportPerMonths.add(report))));

        return reportPerMonths;
    }
}
