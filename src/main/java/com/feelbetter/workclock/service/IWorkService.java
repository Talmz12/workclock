package com.feelbetter.workclock.service;

import com.feelbetter.workclock.model.TimePerMonth;
import com.feelbetter.workclock.model.requests.ReportPerMonth;

import java.util.List;
import java.util.Optional;

public interface IWorkService {
    void handleWorkLog(String user);
    Optional<ReportPerMonth> getUserReportPerMonth(String user, int year, int month);
    List<ReportPerMonth> getAllUsersReportsPerMonth(int year, int month);
}
