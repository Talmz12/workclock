package com.feelbetter.workclock.datasource;

import com.feelbetter.workclock.model.db.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface IEventsCollector extends JpaRepository<Event, Long> {
    @Query(value = "SELECT * FROM events WHERE user=?1 ORDER BY start_time DESC LIMIT 1", nativeQuery = true)
    Event getLatestUpdateForUser(String user);

    @Query(value="SELECT * FROM events WHERE user=?1 AND DATE(start_time) >= ?2 And DATE(end_time) <= ?3", nativeQuery = true)
    List<Event> getUserEventsPerMonth(String user, LocalDateTime startDate, LocalDateTime endDate);
}
