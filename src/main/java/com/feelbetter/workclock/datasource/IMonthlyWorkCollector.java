package com.feelbetter.workclock.datasource;

import com.feelbetter.workclock.model.db.MonthlyWork;
import com.feelbetter.workclock.model.db.MonthlyWorkKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface IMonthlyWorkCollector extends JpaRepository<MonthlyWork, MonthlyWorkKey> {
    @Query(value = "SELECT * FROM monthly_work WHERE year=?1 AND month=?2 AND user=?3", nativeQuery = true)
    MonthlyWork getMonthlyWork(int year, int month, String user);
}
