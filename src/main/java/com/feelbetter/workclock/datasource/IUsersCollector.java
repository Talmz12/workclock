package com.feelbetter.workclock.datasource;

import com.feelbetter.workclock.model.db.Event;
import com.feelbetter.workclock.model.db.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IUsersCollector extends JpaRepository<User, String> {
    @Query(value = "SELECT * FROM users", nativeQuery = true)
    List<User> getLatestUpdateForUser();
}
